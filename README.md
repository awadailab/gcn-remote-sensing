# Semi-supervised Change Detection Building Damage Framework using Graph Convolutional Networks and Urban Domain Knowledge

This repository contains code for the work [BLDNet: A Semi-supervised Change Detection Building Damage Framework using Graph Convolutional Networks and Urban Domain Knowledge](https://arxiv.org/abs/2201.10389) (currently in preprint).

# Setup

Install Anaconda or Miniconda.

Run:
```
conda env create -f environment_tensorflow.yaml
conda env create -f environment_torch.yaml
```

This will create two conda virtual environments. The Tensorflow environment is used only for the Semi-supervised Multiresolution Autoencoder related experiments which was adapted from https://github.com/tanodino/Semi_Supervised_Auto_Encoder.git. All other experiments are executed using the Pytorch environment.

# Use

It is preferred to create the following directory tree in the same directory of the code files to avoid having to modify path variables inside the script files.

```
.
├── weights
├── results
│    ├── Sensitivity GCN AE
│    ├── T-test GCN AE
├── datasets
│   ├── xbd
│   ├── beirut_bldgs
```

Download the xBD dataset from https://xview2.org/ and unzip the content into the `xbd` subdirectory. Run the `bldgs_xbd.py` script to extract building crops from the xBD dataset.

The `exp_settings.json` file contains the experimental configurations such as model hyperparameters and data subsets and can be modified accordingly.

Unfortunately, the Beirut Port Explosion data that we used cannot be shared. However, a set of public images can be downloaded from https://www.maxar.com/open-data/beirut-explosion. The damage annotations are also still not publicly shared by the Open Map Lebanon team (https://openmaplebanon.org/beirut-recovery-map).

# Citation
If you use this work, please cite:

```
@misc{ismail2022bldnet,
      title={BLDNet: A Semi-supervised Change Detection Building Damage Framework using Graph Convolutional Networks and Urban Domain Knowledge}, 
      author={Ali Ismail and Mariette Awad},
      year={2022},
      eprint={2201.10389},
      archivePrefix={arXiv},
      primaryClass={cs.CV}
}
```
